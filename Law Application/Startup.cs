﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Law_Application.Startup))]
namespace Law_Application
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
